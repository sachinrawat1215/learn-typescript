To initialize typescript we have to run command - tsc --init

** By using this command it will start to work on typescript and after that we can use many other method on typescript file.

<!--------------------------------------------------------------------------------------------->

To watch or to run any typescript file we have to run - tsc filename.ts or tsc filename.ts -w

** Here we have given -w flag to automatically run ts file when it found any changes in the file.

<!--------------------------------------------------------------------------------------------->

To set root dir or outer dir we have to use tsconfig.json file to set path of file. Once we set all these then we just have to run - tsc -w

** and now it will compile all the files which is present in the root directory.

<!--------------------------------------------------------------------------------------------->

To exclude or to run only some file we can use include in config file in the last of file.