let character = 'mario';

let age = 20;

let isBlackBelt = false;

// In typescript we can't declare different data type in same variable
// character = 20;
character = 'hello world';
age = 50;
isBlackBelt = true;

// By this way we can declare data type of the parameter
const circ = (dia: number) => {
    return dia * Math.PI;
};

// console.log(circ(20));

// -----------------------------------------------------------------------------------------------------

let names = ['sachin', 'mohan', 'rohan', 'sahil', 20, false];

// In array we can assign same data type
names.push('okay');

// But if it's mixed array then we can declare on those types which are in the array
names.push(20);
names.push(true);
// console.log(names);

let nameObj = {
    name: 'Rohan',
    age: 20,
    employee: true
}

nameObj.name = 'Sohan';

console.log(nameObj);

// We can declare other data type to string value
// nameObj.name = 20;

// We can't declare any other key to that object
// nameObj.skill = 'Node.js';

// --------------------------------------------------------------------------------------------------------------

// By this way we can decide datatype to any variable
let username: string;
let userage: number;
let isLoggedIn: boolean;

// isLoggedIn = 20;

// By this way we can decide data type of array and we have assign blank array so we can use array methods
let nijas: string[] = [];

// By this way we can set multiple data types to an array
let mixed: (string | number | boolean)[] = [];

// By this way we can set type to a variable
let uid: string | number;

// By this way we can set object data type
let ninjaOne: object;

// By this way we can set data type of object values and now it expect it's all keys
let ninjaTwo: {
    name: string,
    age: number,
    beltColor: string
};

// Like this
ninjaTwo = {
    name: 'hello',
    age: 20,
    beltColor: 'white'
};


// ----------------------------------------------------------------- 

// By this way we set any data type value to a variable
let myage : any = 26;

// myage = true;
// myage = 'okay';

// By this way we set any data type to array
let mixedarray : any[] = [];

// By this way we set data type to object key and all key are import
let ninja : {name: any, age: any};

ninja = { name: 'string', age: 20 };

console.log('hello world...');